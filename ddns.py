#!/usr/bin/python3
# -*- encoding:utf-8 -*-

'''
    Simple script for monitoring changes in external IP address and updating
    the A Record in case of changes.

    The arguments required are host, domain and password.
'''
import os, sys
import requests
import re

if not len(sys.argv) == 4:
    sys.exit("This script requires 4 arguments. Please provide host, domain name and password when calling this script")
host, domain, pwd = sys.argv[1], sys.argv[2], sys.argv[3]

# File for storing IP
path = (os.path.dirname(os.path.realpath(__file__)) + '/kept_ip.txt')
if not os.path.isfile(path):
    open(path,'w').close() # create file if it doesn't exist

# Retrieve the last recorded external IP
with open(path, 'r') as ipFile:
    oldIP = ipFile.read().strip()

# Get current external IP address
newIP = os.popen('curl -s4 https://organicdesign.nz/files/ip.php').read().strip()

# Update the domain's A record
if oldIP != newIP:
    url = ('https://dynamicdns.park-your-domain.com/update?host=%s&domain=%s&password=%s&ip=%s' % (host, domain, pwd, newIP))
    recordReset = requests.get(url)

    # Check for errors on the API response
    if recordReset.raise_for_status() == None:
        pattern = '(?<=<errors><Err1>).+?(?=<\/Err1>)'
        if not re.findall(pattern, recordReset.text) == []:
            print(re.findall(pattern, recordReset.text)[0], url, file=sys.stderr)

        # No errors were found, store the IP on the file
        with open(path , 'w') as ipFile:
            ipFile.write(newIP)

    # In case of response error, output URL and error code
    else:
        print(recordReset.status_code, url, file=sys.stderr)
